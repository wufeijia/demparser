package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/dotabuff/yasha"
)

var GLastSec int64 = 0
var GStarted bool = false

var GCollection *NpcStatCollection = NewNpcStatCollection()
var GEvents *EventCollection = NewEventCollection()

const MAX_COORDINATE float64 = 16384

type Coordinate struct {
	X, Y float64
}

type NPCStat struct {
	PEStat  *StatFromPE
	CbtStat *StatFromCbt
}

type StatFromPE struct {
	HP    uint
	MP    float64
	Level int
	LocY  float64
	LocX  float64
}

func (sp *StatFromPE) String() string {
	return fmt.Sprintf("%7d | %7.0f | %3d | %5.0f | %5.0f |",
		sp.HP, sp.MP, sp.Level, sp.LocX, sp.LocY)
}

type StatFromCbt struct {
	Gold        int
	KillHero    int
	KillCreep   int
	KillNeutral int
	Items       []string
}

func (sp *StatFromCbt) String() string {
	return fmt.Sprintf("%7d | %4d | %4d | %4d | %s |",
		sp.Gold, sp.KillHero, sp.KillCreep, sp.KillNeutral,
		strings.Join(sp.Items, ","))
}

func NewNpcStatCollection() *NpcStatCollection {
	nsc := &NpcStatCollection{
		Data: make(map[string]*NPCStat),
	}
	nsc.Data["roshan"] = &NPCStat{
		PEStat: &StatFromPE{
			HP: 9999,
			MP: 9999.0,
		},
	}
	return nsc
}

type NpcStatCollection struct {
	Data map[string]*NPCStat
}

func (nc *NpcStatCollection) get(key string) *NPCStat {
	st, suc := nc.Data[key]
	if suc && st != nil {
		return st
	}

	nst := &NPCStat{
		PEStat: &StatFromPE{
			HP: 0,
		},
		CbtStat: &StatFromCbt{
			Items: make([]string, 0),
		},
	}
	nc.Data[key] = nst
	return nst
}

func (nc *NpcStatCollection) setPEStat(key string, sp *StatFromPE) {
	ns := nc.get(key)
	ns.PEStat = sp
}

func (nc *NpcStatCollection) setCbtStat(key string, sc *StatFromCbt) {
	ns := nc.get(key)
	ns.CbtStat = sc
}

func (nc *NpcStatCollection) addOn(key, field string, value interface{}) {
	switch field {
	case "Gold":
		num := value.(int)
		cs := nc.get(key).CbtStat
		cs.Gold += num
	case "KillHero":
		num := value.(int)
		cs := nc.get(key).CbtStat
		cs.KillHero += num
	case "KillCreep":
		num := value.(int)
		cs := nc.get(key).CbtStat
		cs.KillCreep += num
	case "KillNeutral":
		num := value.(int)
		cs := nc.get(key).CbtStat
		cs.KillNeutral += num
	case "Item":
		item := value.(string)
		cs := nc.get(key).CbtStat
		cs.Items = append(cs.Items, item)
	default:
		panic("no such field support")
	}

}

func (nc *NpcStatCollection) Print(now int64) {

	for k, v := range nc.Data {
		fmt.Printf("%6d | %15s | %s%s\n", now, k, v.PEStat, v.CbtStat)
	}
}

type Event struct {
	Source string
	Target string
	Bhv    string
	Via    string
}

func (e *Event) String() string {
	return fmt.Sprintf("%20s | %20s | %15s | %15s",
		e.Source, e.Target, e.Bhv, e.Via)
}

func NewEventCollection() *EventCollection {
	return &EventCollection{
		Data: make([]*Event, 0),
	}
}

type EventCollection struct {
	Data []*Event
}

func (ec *EventCollection) record(e *Event) {
	ec.Data = append(ec.Data, e)
}

func (ec *EventCollection) clear() {
	ec.Data = make([]*Event, 0)
}

func (ec *EventCollection) Print(now int64) {
	for _, e := range ec.Data {
		fmt.Printf("%6d | %s\n", now, e)
	}
}

func ParsePE(pe *yasha.PacketEntity) {
	if pe.Name == "DT_DOTAGamerulesProxy" {
		gameTime := pe.Values["DT_DOTAGamerules.m_fGameTime"].(float64)
		preGameStarttime := pe.Values["DT_DOTAGamerules.m_flPreGameStartTime"].(float64)
		now := int64((time.Duration(gameTime-preGameStarttime) * time.Second).Seconds())
		if now == 0 {
			GStarted = true
		}

		if now != GLastSec && GStarted {
			GCollection.Print(GLastSec)
			//GEvents.Print(GLastSec)
			//GEvents.clear()
			GLastSec = now
		}
	}

	if isWantedPE(pe.Name) {
		switch {
		case strings.HasPrefix(pe.Name, "DT_DOTA_Unit_"):
			var coord Coordinate
			//if _, ok := pe.Delta["DT_DOTA_BaseNPC.m_vecOrigin"]; ok {
			if _, ok := pe.Values["DT_DOTA_BaseNPC.m_vecOrigin"]; ok {
				coord = coordFromCell(pe)
			}

			var hp uint
			if _, ok := pe.Values["DT_DOTA_BaseNPC.m_iHealth"]; ok {
				hp = pe.Values["DT_DOTA_BaseNPC.m_iHealth"].(uint)
			}

			var mp float64
			if _, ok := pe.Values["DT_DOTA_BaseNPC.m_flMana"]; ok {
				mp = pe.Values["DT_DOTA_BaseNPC.m_flMana"].(float64)
			}

			var level int
			if _, ok := pe.Values["DT_DOTA_BaseNPC.m_iCurrentLevel"]; ok {
				level = pe.Values["DT_DOTA_BaseNPC.m_iCurrentLevel"].(int)
			}

			dpe := &StatFromPE{
				HP:    hp,
				MP:    mp,
				Level: level,
				LocX:  coord.X,
				LocY:  coord.Y,
			}
			name := normName(pe.Name)
			GCollection.setPEStat(name, dpe)
		default:
			var hp uint
			if _, ok := pe.Values["DT_DOTA_BaseNPC.m_iHealth"]; ok {
				hp = pe.Values["DT_DOTA_BaseNPC.m_iHealth"].(uint)
			}
			dpe := &StatFromPE{
				HP: hp,
			}
			name := pe.Values["DT_BaseEntity.m_iName"].(string)
			GCollection.setPEStat(name, dpe)
		}
	}
}

func ParseCbt(tick int, entry yasha.CombatLogEntry) {
	switch log := entry.(type) {
	case *yasha.CombatLogPurchase:
		hero := normName(log.Buyer)
		GCollection.addOn(hero, "Item", log.Item)
	case *yasha.CombatLogBuyback:
		e := &Event{
			Source: string(log.PlayerId),
			Bhv:    "BuyBack",
		}
		GEvents.record(e)
	case *yasha.CombatLogAbility:
		if log.Target != "dota_unknown" {
			e := &Event{
				Source: normName(log.Attacker),
				Target: normName(log.Target),
				Bhv:    "Cast",
				Via:    log.Ability,
			}
			GEvents.record(e)
		}
	case *yasha.CombatLogGold:
		hero := normName(log.Target)
		GCollection.addOn(hero, "Gold", log.Value)
	case *yasha.CombatLogHeal:
		e := &Event{
			Source: normName(log.Source),
			Target: normName(log.Target),
			Bhv:    "Heal",
		}
		GEvents.record(e)
	case *yasha.CombatLogItem:
		if log.UserIsHero && log.Target != "dota_unknown" {
			e := &Event{
				Source: normName(log.User),
				Target: normName(log.Target),
				Bhv:    "useItem",
				Via:    log.Item,
			}
			GEvents.record(e)
		}
	case *yasha.CombatLogDeath:
		switch {
		case log.AttackerIsHero:
			hero := normName(log.Attacker)
			switch {
			case log.TargetIsHero:
				GCollection.addOn(hero, "KillHero", 1)
			case isCreep(log.Target):
				GCollection.addOn(hero, "KillCreep", 1)
			case isNeutral(log.Target):
				GCollection.addOn(hero, "KillNeutral", 1)
			}
			if isRoshan(log.Target) {
				sp := &StatFromPE{
					HP: 0,
					MP: 0,
				}
				GCollection.setPEStat("roshan", sp)
			}
		case log.TargetIsHero:
		}
	}
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Expected a .dem file as argument")
	}

	for _, path := range os.Args[1:] {
		parser := yasha.ParserFromFile(path)

		parser.OnEntityPreserved = ParsePE
		parser.OnCombatLog = ParseCbt
		parser.Parse()
	}
}

var npcPrefixs []string = []string{"DT_DOTA_Unit_Hero_",
	"npc_dota_hero_",
	"DT_DOTA_Unit_",
	"npc_dota_"}

var wantedPEs []string = []string{"DT_DOTA_Unit_Hero",
	"DT_DOTA_Unit_Courier",
	"DT_DOTA_BaseNPC_Tower"}

var wantedNpcs []string = []string{"DT_DOTA_Unit_Hero_",
	"npc_dota_hero_",
	"npc_dota_badguys_tower",
	"npc_dota_goodguys_tower",
	"npc_dota_roshan",
}

func isWantedPE(name string) bool {
	for _, prefix := range wantedPEs {
		if strings.HasPrefix(name, prefix) {
			return true
		}
	}
	return false
}

func isWantedNpc(name string) bool {
	for _, prefix := range wantedNpcs {
		if strings.HasSuffix(name, prefix) {
			return true
		}
	}
	return false
}

func isRoshan(target string) bool {
	return strings.HasSuffix(target, "roshan")
}

func isCreep(target string) bool {
	return strings.HasPrefix(target, "npc_dota_creep_")
}

func isHero(target string) bool {
	return strings.HasPrefix(target, "npc_dota_hero_")
}

func isNeutral(target string) bool {
	return strings.HasPrefix(target, "npc_dota_neutral_")
}

func coordFromCell(pe *yasha.PacketEntity) Coordinate {
	cellbits, ok := pe.Values["DT_BaseEntity.m_cellbits"].(int)
	if !ok {
		return Coordinate{X: 0, Y: 0}
	}
	cellWidth := float64(uint(1) << uint(cellbits))

	var cX, cY, vX, vY float64

	if vO2, ok := pe.Values["DT_DOTA_BaseNPC.m_vecOrigin"].(*yasha.Vector2); ok {
		cX = float64(pe.Values["DT_DOTA_BaseNPC.m_cellX"].(int))
		cY = float64(pe.Values["DT_DOTA_BaseNPC.m_cellY"].(int))
		vX, vY = vO2.X, vO2.Y
	} else {
		vO3 := pe.Values["DT_BaseEntity.m_vecOrigin"].(*yasha.Vector3)
		cX = float64(pe.Values["DT_BaseEntity.m_cellX"].(int))
		cY = float64(pe.Values["DT_BaseEntity.m_cellY"].(int))
		vX, vY = vO3.X, vO3.Y
	}

	x := ((cX * cellWidth) - MAX_COORDINATE) + vX
	y := ((cY * cellWidth) - MAX_COORDINATE) + vY

	return Coordinate{X: x, Y: y}
}

func normName(name string) string {
	for _, prefix := range npcPrefixs {
		if len(strings.Split(name, prefix)) == 1 {
			continue
		}

		mid := strings.Split(name, prefix)[1]
		ml := strings.Split(mid, "_")
		if len(ml) == 1 {
			return strings.ToLower(ml[0])
		}

		var ret string
		for _, m := range ml {
			ret += strings.Title(m)
		}
		return strings.ToLower(ret)
	}

	return strings.ToLower(name)
}
